from database import Base
from sqlalchemy import Column, Integer, String

# SQLalchemy is a ORM: Object Relational Mapper
# What it does is that is maps (translates) an object from its representation in python to SQL and vice versa

class Course(Base):
    __tablename__ = 'CourseMaster'  # should actually be named 'courses'
    course_uid = Column(Integer, primary_key=True, autoincrement=True)
    course_id = Column(String(10), unique=True)  # should be refactored away to use UID instead
    course_name = Column(String(50))
    course_text = Column(String(255))  # should be course_description to match spec

    def __init__(self, id, name, description):
        self.course_id = id
        self.course_name = name
        self.course_text = description

    def __repr__(self):
        return 'Course UID: {}\nCourse ID: {}\nCourse Name:{}\nCourse Desc:{}' \
            .format(self.course_uid, self.course_id, self.course_name, self.course_text)


class Student(Base):
    __tablename__ = 'StudentMaster'
    # common sense here did not translate to courses table, why?
    student_id = Column(Integer, primary_key=True, autoincrement=True)
    student_firstname = Column(String(50))
    student_lastname = Column(String(50))
    student_gender = Column(String(10))  # that's right kids, use a varchar to store binary input
    student_address = Column(String(256))  # 1 more than course desc. sure.
    student_phone = Column(String(11))
    course_id = Column(String(10))  # oh sure. that's the relational part right? gotcha.

    def __init__(self, first_name, last_name, gender, address, phone, course_id):
        self.student_firstname = first_name
        self.student_lastname = last_name
        self.student_gender = gender
        self.student_address = address
        self.student_phone = phone
        self.course_id = course_id

    def __repr__(self):
        return 'ID: {}\nFirst: {}\nLast: {}\nGender: {}\nAddress: {}\nPhone: {}\nCourse: {}' \
            .format(self.student_id, self.student_firstname, self.student_lastname, self.student_gender,
                    self.student_address, self.student_phone, self.course_id)
