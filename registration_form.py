from flask_wtf import FlaskForm
from wtforms import StringField, RadioField, SelectField, TextAreaField, SubmitField
from wtforms.validators import DataRequired, Length
from database import db_session
from models import Course


class RegistrationForm(FlaskForm):
    firstname = StringField(label='First Name', validators=[DataRequired(message="This field is required!"),
                                                       Length(max=50, message="Length must be shorter than 50!")])

    lastname = StringField(label='Last Name', validators=[Length(max=50, message="Length must be shorter than 50!")])

    gender = RadioField(label='Gender', validators=[DataRequired(message="This field is required!")],
                        choices=[('Male', 'Male'), ('Female', 'Female')])

    residential_address = TextAreaField(label='Residential Address',
                                        validators=[DataRequired(message="This field is required!")])

    phone_number = StringField(label='Phone Number', validators=[Length(max=11)])

    course = SelectField(label='Course', validators=[DataRequired(message="This field is required!")])

    submit = SubmitField(label='Register')

